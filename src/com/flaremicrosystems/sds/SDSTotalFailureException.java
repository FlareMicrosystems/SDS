package com.flaremicrosystems.sds;

public class SDSTotalFailureException extends RuntimeException {

	public SDSTotalFailureException(Exception e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
