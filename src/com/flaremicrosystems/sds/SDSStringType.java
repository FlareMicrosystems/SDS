package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class SDSStringType extends SDSObject<String>{
	
	public SDSStringType() {
		super();
	}
	public SDSStringType(String value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeUTF(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readUTF();
		return this;
	}
	@Override
	public int getByteLength() {
		try {
			return value.getBytes("UTF-8").length;
		} catch (UnsupportedEncodingException e) {
			return value.getBytes().length;
		}
	}
}
