package com.flaremicrosystems.sds;

public class SDSNullType<T> extends SDSObject<T>{
	@Override
	public int getByteLength() {
		return 0;
	}
	@Override
	public boolean isNull()
	{
		return true;
	}
}
