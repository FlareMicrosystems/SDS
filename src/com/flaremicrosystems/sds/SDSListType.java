package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class SDSListType<T> extends SDSObject<ArrayList<SDSObject<T>>> implements Iterable<T> {

	public SDSListType() {
		this.value = new ArrayList<SDSObject<T>>();
	}

	public void store(DataOutputStream out) throws IOException {
		super.store(out);
		out.writeInt(value.size());
		Iterator<SDSObject<T>> keyIter = value.iterator();
		while (keyIter.hasNext()) {
			SDSObject<T> k = keyIter.next();
			k.store(out);
		}
	}

	@SuppressWarnings("unchecked")
	public SDSObject<?> load(DataInputStream in) throws IOException {
		int len = in.readInt();
		for (int i = 0; i < len; i++) {
			try {
				SDSObject<?> value = SDSObject.readObject(in);
				this.value.add((SDSObject<T>) value);
			} catch (SDSUndefinedException e) {
				e.printStackTrace();
			}
		}
		return this;
	}

	@Override
	public int getByteLength() {
		int length = 4;
		for (SDSObject<?> obj : value)
			length += OPCODE_BYTES + LENGTH_BYTES + obj.getByteLength();
		return length;
	}

	public void add(SDSObject<T> val) {
		value.add(val);
	}

	@SuppressWarnings("unchecked")
	public Iterator<T> iterator() {
		return (Iterator<T>) value.iterator();
	}

	public int size() {
		return value.size();
	}

	public SDSObject<T> get(int index) {
		return value.get(index);
	}

	public T[] asArray(T[] container) {
		for(int i = 0; i < container.length && i < value.size(); i++)
		{
			container[i] = value.get(i).value;
		}
		return container;
	}
}
