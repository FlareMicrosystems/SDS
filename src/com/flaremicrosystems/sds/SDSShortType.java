package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSShortType extends SDSObject<Short>{
	
	public SDSShortType() {
		super();
	}
	public SDSShortType(Short value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeShort(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readShort();
		return this;
	}
	@Override
	public int getByteLength() {
		return 2;
	}
}
