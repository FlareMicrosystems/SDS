package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSLongType extends SDSObject<Long>{
	
	public SDSLongType() {
		super();
	}
	public SDSLongType(Long value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeLong(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readLong();
		return this;
	}
	@Override
	public int getByteLength() {
		return 8;
	}
}
