package com.flaremicrosystems.sds;

public class SDSUndefinedException extends Exception {

	public SDSUndefinedException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
