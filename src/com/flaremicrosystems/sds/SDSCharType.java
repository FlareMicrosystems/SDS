package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSCharType extends SDSObject<Character>{
	
	public SDSCharType() {
		super();
	}
	public SDSCharType(Character value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeChar(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readChar();
		return this;
	}
	@Override
	public int getByteLength() {
		return 2;
	}
}
