package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSBinaryDataType<T> extends SDSObject<byte[]>{
	
	public SDSBinaryDataType()
	{
	}
	
	public SDSBinaryDataType(byte[] data)
	{
		this.value = data;
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeInt(value.length);
		out.write(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException, SDSUndefinedException
	{
		value = new byte[in.readInt()];
		return this;
	}

	@Override
	public int getByteLength() {
		return 4 + value.length;
	}
}
