package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSFloatType extends SDSObject<Float>{
	
	public SDSFloatType() {
		super();
	}
	public SDSFloatType(Float value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeFloat(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readFloat();
		return this;
	}
	@Override
	public int getByteLength() {
		return 4;
	}
}
