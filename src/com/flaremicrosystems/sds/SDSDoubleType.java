package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSDoubleType extends SDSObject<Double>{
	
	public SDSDoubleType() {
		super();
	}
	public SDSDoubleType(Double value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeDouble(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readDouble();
		return this;
	}
	@Override
	public int getByteLength() {
		return 8;
	}
}
