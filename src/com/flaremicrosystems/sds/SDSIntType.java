package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSIntType extends SDSObject<Integer>{
	
	public SDSIntType() {
		super();
	}
	public SDSIntType(Integer value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeInt(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readInt();
		return this;
	}
	@Override
	public int getByteLength() {
		return 4;
	}
}
