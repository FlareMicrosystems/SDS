package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSByteType extends SDSObject<Byte>{
	
	public SDSByteType() {
		super();
	}
	public SDSByteType(Byte value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeByte(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readByte();
		return this;
	}
	@Override
	public int getByteLength() {
		return 1;
	}
}
