package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SDSBooleanType extends SDSObject<Boolean>{
	
	public SDSBooleanType() {
		super();
	}
	public SDSBooleanType(Boolean value) {
		super(value);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeBoolean(value);
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		value = in.readBoolean();
		return this;
	}
	@Override
	public int getByteLength() {
		return 1;
	}
	
}
