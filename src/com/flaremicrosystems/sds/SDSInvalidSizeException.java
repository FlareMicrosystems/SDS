package com.flaremicrosystems.sds;

public class SDSInvalidSizeException extends RuntimeException {

	public SDSInvalidSizeException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
