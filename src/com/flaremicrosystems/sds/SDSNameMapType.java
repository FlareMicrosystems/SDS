package com.flaremicrosystems.sds;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.TreeMap;

public class SDSNameMapType extends SDSObject<TreeMap<String, SDSObject<?>>>{
	public SDSNameMapType()
	{
		this.value = new TreeMap<String, SDSObject<?>>();
	}
	
	public Boolean getBooleanValue(String key, Boolean fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSBooleanType))
			return fallback;
		else return (Boolean) obj.value;
	}
	
	public Byte getByteValue(String key, Byte fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSByteType))
			return fallback;
		else return (Byte) obj.value;
	}
	
	public Short getShortValue(String key, Short fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSShortType))
			return fallback;
		else return (Short) obj.value;
	}
	
	public Integer getIntValue(String key, Integer fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSIntType))
			return fallback;
		else return (Integer) obj.value;
	}
	
	public Character getCharacterValue(String key, Character fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSCharType))
			return fallback;
		else return (Character) obj.value;
	}
	
	public Long getLongValue(String key, Long fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSLongType))
			return fallback;
		else return (Long) obj.value;
	}
	
	public Float getFloatValue(String key, Float fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSFloatType))
			return fallback;
		else return (Float) obj.value;
	}
	
	public Double getDoubleValue(String key, Double fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSDoubleType))
			return fallback;
		else return (Double) obj.value;
	}
	
	public String getStringValue(String key, String fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSStringType))
			return fallback;
		else return (String) obj.value;
	}
	
	public SDSListType<?> getArrayValue(String key, SDSListType<?> fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSListType))
			return fallback;
		else return (SDSListType<?>)obj;
	}
	
	public SDSNameMapType getNameMapping(String key, SDSNameMapType fallback)
	{
		SDSObject<?> obj = this.value.get(key);
		if(obj == null || !(obj instanceof SDSNameMapType))
			return fallback;
		else return (SDSNameMapType)obj;
	}
	
	public SDSObject<?> getSDSObject(String key)
	{
		return this.value.get(key);
	}
	
	public void store(DataOutputStream out) throws IOException
	{
		super.store(out);
		out.writeInt(value.size());
		Iterator<String> keyIter = value.keySet().iterator();
		while(keyIter.hasNext())
		{
			String k = keyIter.next();
			SDSObject<?> v = value.get(k);
			out.writeUTF(k);
			v.store(out);
		}
	}
	
	public SDSObject<?> load(DataInputStream in) throws IOException
	{
		int len = in.readInt();
		for(int i = 0; i < len; i++)
		{
			String key = in.readUTF();
			try {
				SDSObject<?> value = SDSObject.readObject(in);
				this.value.put(key, value);
			} catch (SDSUndefinedException e) {
				e.printStackTrace();
			} 
		}
		return this;
	}

	public void put(String name, SDSObject<?> sdsObject) {
		if(sdsObject == null)
			value.remove(name);
		else value.put(name, sdsObject);
	}
	
	public void delete(String name)
	{
		put(name, null);
	}

	@Override
	public int getByteLength() {
		int length = 4;
		Iterator<String> keyIter = value.keySet().iterator();
		while(keyIter.hasNext())
		{
			String k = keyIter.next();
			try {
				length += k.getBytes("UTF-8").length;
			} catch (UnsupportedEncodingException e) {
				length += k.getBytes().length;
			}
			SDSObject<?> v = value.get(k);
			length += OPCODE_BYTES + LENGTH_BYTES + v.getByteLength();
		}
		return length;
	}

	public void putBoolean(String name, boolean val) {
		this.put(name, new SDSBooleanType(val));
	}
	public void putByte(String name, byte val) {
		this.put(name, new SDSByteType(val));
	}
	public void putShort(String name, short val) {
		this.put(name, new SDSShortType(val));
	}
	public void putChar(String name, char val) {
		this.put(name, new SDSCharType(val));
	}
	public void putInt(String name, int val) {
		this.put(name, new SDSIntType(val));
	}
	public void putLong(String name, long val) {
		this.put(name, new SDSLongType(val));
	}
	public void putFloat(String name, float val) {
		this.put(name, new SDSFloatType(val));
	}
	public void putDouble(String name, double val) {
		this.put(name, new SDSDoubleType(val));
	}
	public void putString(String name, String val) {
		this.put(name, new SDSStringType(val));
	}

	public boolean hasOfType(String name, Class<?> tester) {
		return this.value.containsKey(name) && tester.isInstance(this.value.get(name));
	}


	
}
